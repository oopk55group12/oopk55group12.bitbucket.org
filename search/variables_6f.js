var searchData=
[
  ['offset',['Offset',['http://qt-project.org/doc/qt-4.8/qiodevice-qt3.html#Offset-typedef',1,'QIODevice']]],
  ['old_5fmode',['old_mode',['../a00004.html#af42008cbd585845dc3490e87bc23a956',1,'Canvas']]],
  ['old_5fzoomfactor',['old_zoomFactor',['../a00004.html#ac2e07bf229fa306eef07031a08807415',1,'Canvas']]],
  ['on',['on',['../a00024.html#a2704b12058fcb08539d46d748a69a6b8',1,'SetRenderHint']]],
  ['openglfeatures',['OpenGLFeatures',['http://qt-project.org/doc/qt-4.8/qglfunctions.html#OpenGLFeatures-typedef',1,'QGLFunctions']]],
  ['openglversionflags',['OpenGLVersionFlags',['http://qt-project.org/doc/qt-4.8/qglformat.html#OpenGLVersionFlags-typedef',1,'QGLFormat']]],
  ['openmode',['OpenMode',['http://qt-project.org/doc/qt-4.8/qiodevice.html#OpenMode-typedef',1,'QIODevice']]],
  ['oppairlist',['opPairList',['../a00018.html#a49ad7aff921e09acdb35070718eace78',1,'Player']]],
  ['ops',['ops',['../a00014.html#a8b67f2b2162969cc40f539baae1cae38',1,'OpPair']]],
  ['optimizationflags',['OptimizationFlags',['http://qt-project.org/doc/qt-4.8/qgraphicsview.html#OptimizationFlags-typedef',1,'QGraphicsView']]],
  ['options',['Options',['http://qt-project.org/doc/qt-4.8/qfiledialog.html#Options-typedef',1,'QFileDialog']]],
  ['orientations',['Orientations',['http://qt-project.org/doc/qt-4.8/qt.html#Orientations-typedef',1,'Qt']]],
  ['originalsize',['originalSize',['../a00018.html#a6eba6426f7a052811d623e0d1c832c2a',1,'Player']]]
];
