var searchData=
[
  ['geometry',['geometry',['http://qt-project.org/doc/qt-4.8/qwidget.html#geometry-prop',1,'QWidget::geometry()'],['http://qt-project.org/doc/qt-4.8/qgraphicswidget.html#geometry-prop',1,'QGraphicsWidget::geometry()']]],
  ['gesturecancelpolicy',['gestureCancelPolicy',['http://qt-project.org/doc/qt-4.8/qgesture.html#gestureCancelPolicy-prop',1,'QGesture']]],
  ['gesturetype',['gestureType',['http://qt-project.org/doc/qt-4.8/qgesture.html#gestureType-prop',1,'QGesture']]],
  ['globalrestorepolicy',['globalRestorePolicy',['http://qt-project.org/doc/qt-4.8/qstatemachine.html#globalRestorePolicy-prop',1,'QStateMachine']]],
  ['globalstrut',['globalStrut',['http://qt-project.org/doc/qt-4.8/qapplication.html#globalStrut-prop',1,'QApplication']]],
  ['gridsize',['gridSize',['http://qt-project.org/doc/qt-4.8/qlistview.html#gridSize-prop',1,'QListView']]],
  ['gridstyle',['gridStyle',['http://qt-project.org/doc/qt-4.8/qtableview.html#gridStyle-prop',1,'QTableView']]],
  ['gridvisible',['gridVisible',['http://qt-project.org/doc/qt-4.8/qcalendarwidget.html#gridVisible-prop',1,'QCalendarWidget']]],
  ['gridx',['gridX',['http://qt-project.org/doc/qt-4.8/q3iconview.html#gridX-prop',1,'Q3IconView']]],
  ['gridy',['gridY',['http://qt-project.org/doc/qt-4.8/q3iconview.html#gridY-prop',1,'Q3IconView']]]
];
