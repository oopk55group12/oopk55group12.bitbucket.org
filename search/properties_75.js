var searchData=
[
  ['undoavailable',['undoAvailable',['http://qt-project.org/doc/qt-4.8/qlineedit.html#undoAvailable-prop',1,'QLineEdit']]],
  ['undodepth',['undoDepth',['http://qt-project.org/doc/qt-4.8/q3textedit.html#undoDepth-prop',1,'Q3TextEdit']]],
  ['undolimit',['undoLimit',['http://qt-project.org/doc/qt-4.8/qundostack.html#undoLimit-prop',1,'QUndoStack']]],
  ['undoredoenabled',['undoRedoEnabled',['http://qt-project.org/doc/qt-4.8/qtextdocument.html#undoRedoEnabled-prop',1,'QTextDocument::undoRedoEnabled()'],['http://qt-project.org/doc/qt-4.8/qtextbrowser.html#undoRedoEnabled-prop',1,'QTextBrowser::undoRedoEnabled()'],['http://qt-project.org/doc/qt-4.8/qtextedit.html#undoRedoEnabled-prop',1,'QTextEdit::undoRedoEnabled()'],['http://qt-project.org/doc/qt-4.8/q3textedit.html#undoRedoEnabled-prop',1,'Q3TextEdit::undoRedoEnabled()'],['http://qt-project.org/doc/qt-4.8/qplaintextedit.html#undoRedoEnabled-prop',1,'QPlainTextEdit::undoRedoEnabled()']]],
  ['unifiedtitleandtoolbaronmac',['unifiedTitleAndToolBarOnMac',['http://qt-project.org/doc/qt-4.8/qmainwindow.html#unifiedTitleAndToolBarOnMac-prop',1,'QMainWindow']]],
  ['uniformitemsizes',['uniformItemSizes',['http://qt-project.org/doc/qt-4.8/qlistview.html#uniformItemSizes-prop',1,'QListView']]],
  ['uniformrowheights',['uniformRowHeights',['http://qt-project.org/doc/qt-4.8/qtreeview.html#uniformRowHeights-prop',1,'QTreeView']]],
  ['updateinterval',['updateInterval',['http://qt-project.org/doc/qt-4.8/qtimeline.html#updateInterval-prop',1,'QTimeLine']]],
  ['updatesenabled',['updatesEnabled',['http://qt-project.org/doc/qt-4.8/qwidget.html#updatesEnabled-prop',1,'QWidget']]],
  ['url',['url',['http://qt-project.org/doc/qt-4.8/qdeclarativecomponent.html#url-prop',1,'QDeclarativeComponent::url()'],['http://qt-project.org/doc/qt-4.8/qgraphicswebview.html#url-prop',1,'QGraphicsWebView::url()'],['http://qt-project.org/doc/qt-4.8/qwebframe.html#url-prop',1,'QWebFrame::url()'],['http://qt-project.org/doc/qt-4.8/qwebview.html#url-prop',1,'QWebView::url()']]],
  ['usedesignmetrics',['useDesignMetrics',['http://qt-project.org/doc/qt-4.8/qtextdocument.html#useDesignMetrics-prop',1,'QTextDocument']]],
  ['usesbigpixmaps',['usesBigPixmaps',['http://qt-project.org/doc/qt-4.8/q3mainwindow.html#usesBigPixmaps-prop',1,'Q3MainWindow']]],
  ['usesdropdown',['usesDropDown',['http://qt-project.org/doc/qt-4.8/q3actiongroup.html#usesDropDown-prop',1,'Q3ActionGroup']]],
  ['usesscrollbuttons',['usesScrollButtons',['http://qt-project.org/doc/qt-4.8/qtabwidget.html#usesScrollButtons-prop',1,'QTabWidget::usesScrollButtons()'],['http://qt-project.org/doc/qt-4.8/qtabbar.html#usesScrollButtons-prop',1,'QTabBar::usesScrollButtons()']]],
  ['usestextlabel',['usesTextLabel',['http://qt-project.org/doc/qt-4.8/q3mainwindow.html#usesTextLabel-prop',1,'Q3MainWindow']]]
];
